#!/usr/bin/env perl

use strict;
use warnings;

$| = 1;

my ( $inFile, $blastFile ) = @ARGV;

unless ( $inFile && $blastFile )
{
	die "
USAGE:
$0 databaseFastaFile blastFile
";
}

open( IN, $inFile ) || die "ERROR: can't open $inFile\n\a";
my %namesHash;
my $i = 0;
foreach my $line (<IN>)
{
	chomp $line;
	if ( $line =~ /^>(\S+)/ )
	{
		my $short_name = $1;
		$line =~ s/^>//;

		#       $line =~ s/_/\t/g;
		#       $line =~ s/;//g;
		$namesHash{$i} = $line;
		$i++;

		#        $namesHash{$short_name}=$line;
	}
}
close IN;

open( IN, $blastFile ) || die "ERROR: can't open $blastFile\n\a";
foreach my $line (<IN>)
{
	chomp $line;
	if ( $line =~ /^(\S+)\s+(\S+)\s+(.+)$/ )
	{
		if ( exists $namesHash{$2} )
		{

			#           @names = split(";", $namesHash{$2})
			print $1, "\t", $namesHash{$2}, "\t", $3, "\n";
		}
		else { die "$line\nNo database sequence for $2, aborting\a\n"; }
	}
	else
	{
		die "$line\nError reading input file, aborting\a\n";
	}
}
close IN;

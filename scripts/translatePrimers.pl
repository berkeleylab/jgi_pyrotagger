#!/usr/bin/env perl

use strict;
use warnings;

$| = 1;

my ($inFile) = @ARGV;

unless ($inFile)
{
	die "
USAGE:
$0 inFile
";
}

open( IN, $inFile ) || die "ERROR: can't open $inFile\n\a";

foreach my $line (<IN>)
{
	chomp $line;
	if ( $line =~ /^(\S+)\s+(\S+)/ )
	{
		my $primer  = $2;
		my $name    = $1;
		my @primers = &unWobble($primer);
		foreach my $primer_ (@primers)
		{
			print $name, "\t", $primer_, "\n";
		}
	}
}
close IN;

sub unWobble
{
	my ($primer) = @_;

	my @primers;
	if ( $primer =~ /U/ ) { @primers = &replaceChar( $primer, "U", "T" ); }
	elsif ( $primer =~ /B/ )
	{
		@primers = &replaceChar( $primer, "B", "C", "G", "T" );
	}
	elsif ( $primer =~ /D/i )
	{
		@primers = &replaceChar( $primer, "D", "T", "C", "G" );
	}
	elsif ( $primer =~ /H/i )
	{
		@primers = &replaceChar( $primer, "H", "T", "C", "A" );
	}
	elsif ( $primer =~ /K/i ) {
		@primers = &replaceChar( $primer, "K", "T", "G" );
	}
	elsif ( $primer =~ /M/i ) {
		@primers = &replaceChar( $primer, "M", "A", "C" );
	}
	elsif ( $primer =~ /N/i )
	{
		@primers = &replaceChar( $primer, "N", "A", "T", "C", "G" );
	}
	elsif ( $primer =~ /R/i ) {
		@primers = &replaceChar( $primer, "R", "G", "A" );
	}
	elsif ( $primer =~ /S/i ) {
		@primers = &replaceChar( $primer, "S", "C", "G" );
	}
	elsif ( $primer =~ /V/i )
	{
		@primers = &replaceChar( $primer, "V", "A", "C", "G" );
	}
	elsif ( $primer =~ /W/i ) {
		@primers = &replaceChar( $primer, "W", "A", "T" );
	}
	elsif ( $primer =~ /X/i )
	{
		@primers = &replaceChar( $primer, "X", "A", "T", "C", "G" );
	}
	elsif ( $primer =~ /Y/i ) {
		@primers = &replaceChar( $primer, "Y", "T", "C" );
	}

	#    elsif($primer =~ /[ATCG]/)
	#    { die "Error: unknown characters in primer $primer\n";}
	else
	{
		push( @primers, $primer );
	}
	return @primers;
}

sub replaceChar
{
	my ( $primer, $char, @list ) = @_;
	my @primers;
	foreach my $charR (@list)
	{
		my $primer1 = $primer;
		$primer1 =~ s/$char/$charR/i;
		push( @primers, &unWobble($primer1) );
	}
	return @primers;
}

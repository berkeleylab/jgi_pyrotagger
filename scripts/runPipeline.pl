#!/usr/bin/env perl

############################################################################################
#
# Pyrotags processing pipeline
#
# By Victor Kunin, vkunin@lbl.gov
#
# Feb 2009
#
############################################################################################

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Modueles -=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-

use strict;
use warnings;
use MIME::Lite;
use FindBin qw($Bin);
use lib "$Bin/../lib";
use mapClusters;
use lib "$Bin/../perl/share/perl/5.10.1";
use Config::Simple;

# CONFIG
my %Config;
Config::Simple->import_from( "$Bin/../pyrotagger.ini", \%Config );
die("phyloBlastDB not defined\n") unless exists( $Config{phyloBlastDB} );
die("cgi_url not defined\n")      unless exists( $Config{cgi_url} );
die("bin_dir not defined\n") unless exists( $Config{bin_dir} );
die("bin_dir does not exist\n") unless -d $Config{bin_dir};

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Parameters -=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-
my $USAGE = "
USAGE:
$0 params
Compulsory parameters:
-f file containing tab-delimited rows, in each row primer, fasta and/or qual files 
-p prefix for file names
-s minSeqSize 
-q threshold for fraction of low quality bases, in persent
-i sequence identity threshold for clustering, in percent

Optional parameters:
-c file containing a concatenation of trimmed fasta files described in -f file
-d dir - run the program in dir directory
-e email - to send results over email
-a u - use Uclust algorithm instead of PyroClust. This requires Uclust installation, and is remarkably faster, although PyroClust may be more accurate.

Example usage:
$0 -f tab_delimeted_files_index.txt -p MyData_220_10_uclust -s 220 -q 10 -a u -e myemail\@mydomain.com -d `pwd` >& error_log.txt &

";

my ( $fastaFile, $qualFile, $primersFile, $minSeqSize, $dir, $email,
	$primers_and_fasta_list_file, $combined_fasta );
my ($file_names_file, $prefix, $finalQualFile,
	$qualThresh, $clusterThresh, $clusteringAlgorithm );

my $origParams = "@ARGV";
while ( my $arg = shift @ARGV )
{
	if    ( $arg eq "-f" ) { $primers_and_fasta_list_file = shift @ARGV; }
	elsif ( $arg eq "-c" ) { $combined_fasta              = shift @ARGV; }
	elsif ( $arg eq "-p" ) { $prefix                      = shift @ARGV; }
	elsif ( $arg eq "-s" ) { $minSeqSize                  = shift @ARGV; }
	elsif ( $arg eq "-d" ) { $dir                         = shift @ARGV; }
	elsif ( $arg eq "-e" ) { $email                       = shift @ARGV; }
	elsif ( $arg eq "-q" ) { $qualThresh                  = shift @ARGV; }
	elsif ( $arg eq "-i" ) { $clusterThresh               = shift @ARGV; }
	elsif ( $arg eq "-a" ) { $clusteringAlgorithm         = shift @ARGV; }
	else                   
    { 
        my $val = @ARGV ? shift @ARGV : '';
        die("ERROR: Invalid argument, $arg $val\n\nParameters received: $origParams\n\n$USAGE");
    }
}

unless ( $primers_and_fasta_list_file && $minSeqSize && $prefix ) {
	die $USAGE;
}
die("Invalid quality threshold: $qualThresh\n") unless $qualThresh =~ /^\d+$/;
die("Invalid clustering threshold: $clusterThresh\n") unless $clusterThresh =~ /^9\d$/;
die("Invalid clustering algorithm: $clusteringAlgorithm\n") unless $clusteringAlgorithm =~ /^[pu]$/;

$primersFile = $prefix . ".primers";

my $original_dir;
my $cluster_dir;

chdir $dir;

unless ($combined_fasta)
{
	$qualFile  = $prefix . ".fasta.qual";
	$fastaFile = $prefix . ".fasta";

	if ($dir)
	{
		$primersFile = $dir . "/" . $prefix . ".primers";
		$qualFile    = $dir . "/" . $prefix . ".fasta.qual";
		$fastaFile   = $dir . "/" . $prefix . ".fasta";
	}

	if ( -e $primersFile || -e $fastaFile || -e $qualFile )
	{
		die "
ERROR:
Couldn't create one or more of the following files, file exists:
$fastaFile
$qualFile
$primersFile
";
	}
}
if ( -e $primersFile )
{
	die "
ERROR:
Couldn't create $primersFile file exists.
";
}

open( LIST, $primers_and_fasta_list_file )
  || die "ERROR: can't open $primers_and_fasta_list_file\n";
my $fileFound;
my $trimmedPrimerFastaMapFile = $dir . "/" . $prefix . "_primers2fasta.trimmed";
open( TrimmedMap, "> $trimmedPrimerFastaMapFile" )
  || die "ERROR: can't write to $trimmedPrimerFastaMapFile\n";
my $i = 0;
if ($combined_fasta)
{
	print STDERR
	  "Assuming that you already done the trimming and wobble resolution\n";
}

foreach my $line (<LIST>)
{
	$i++;
	chomp $line;
	my ( $primers, $fasta, $qual ) = split( "\t", $line );
	if ( $primers && $fasta )
	{
		my $primers_unwabbled;

		unless ($combined_fasta
		  ) # assuming that if you have a combined fasta, you already done the wabble thing.
		{
			$primers_unwabbled = $dir . "/" . $prefix . "." . $i . ".primers";
			my $com = "$Bin/translatePrimers.pl $primers > $primers_unwabbled";

			#	    &executeCom($com);
			open( COM, "$com |" );
			close COM;
		}
		else
		{
			$primers_unwabbled = $primers;
		}
		my $com = "cat $primers_unwabbled >> $primersFile";
		system $com;

		#	&executeCom($com);
		print TrimmedMap $primers_unwabbled, "\t";
		$fileFound = 1;
	}
	if ($combined_fasta)
	{
		undef $qualFile;
		$fastaFile = $combined_fasta;
		print TrimmedMap $fasta, "\n";

		#	$fileFound=1;
	}
	elsif ($qual)
	{
		my $trimmedFasta = &lucy_trim( $fasta, $qual, $Config{bin_dir} );
		print TrimmedMap $trimmedFasta, "\n";
		my $com = "cat $trimmedFasta  >> $fastaFile";
		&executeCom($com);
		my $finalQualFile = $fastaFile . ".qual";
		my $trimmedQual   = $trimmedFasta . ".qual";
		$com = "cat $trimmedQual >> $finalQualFile";
		&executeCome($com);
	}
	elsif ($fasta)
	{
		print TrimmedMap $fasta, "\n";
		undef $qualFile;
		my $com = "cat $dir/$fasta  >> $fastaFile";
		&executeCom($com);
		my $trimmedQual = $dir . "/" . $fasta . ".qual";
		print STDERR "Checking for presence of $trimmedQual ...";
		my $finalQualFile = $fastaFile . ".qual";
		if ( -s $trimmedQual )
		{
			print STDERR "present\n";
			my $com = "cat $trimmedQual >> $finalQualFile";
			&executeCom($com);
		}
		else { print STDERR "absent\n"; }
	}
}
close TrimmedMap;
close LIST;

#exit;

unless ($fileFound)
{
	die "ERROR: could not see primers and fasta files\n";
}

#-=-=-=-=-=-=-=-=-=-=--=-=-=-=--=-=-=-=- Constants -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

## FIXIT here:
my $trimCluster         = "$Bin/trimQualityLengthAndDereplicate.pl";
my $filterCommand       = "$Bin/filterByIdentity.pl";
my $getFastaCommand     = "$Bin/getClusterReps4Superclusters.pl";
my $produceTableCommand = "$Bin/produceTable.pl";
my $lucyPrep            = "$Bin/lucyTrim.sh";
my $pyroclustProg       = "$Bin/pyroclust.pl";

#my $vmatchDir = '/home/asczyrba/bin/';
#my $mkVtree   = $vmatchDir . 'mkvtree -dna -pl -allout -v ';
#my $vmatch    = $vmatchDir . 'vmatch -d ';

my $clustersFile            = $fastaFile . ".clusters";
my $seqsFile                = $fastaFile . ".clusters.seq";
my $superClustersFile       = $prefix . ".superclusters";
my $repsFile                = $prefix . ".clusterRepresentatives.fasta";
my $datasets2reads          = $prefix . ".datasets2reads.txt";
my $unifracInput            = $prefix . ".unifrac.txt";
my $clusters2reads          = $prefix . ".clusters2reads.txt";
my $clusters2readsChimeras  = $prefix . ".clusters2reads.chimeras.txt";
my $tableFile               = $prefix . ".cluster_classification.xls";
my $fractions_file          = $prefix . ".cluster_classification_percent.xls";
my $phyloGraphFile          = $prefix . ".phylum_classification.xls";
my $phyloGraphFractionsFile = $prefix . ".phylum_classification_percent.xls";
my $qcFastaFile             = $prefix . ".fasta";
my $qcQualFile              = $prefix . ".fasta.qual";

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Workflow -=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-

$| = 1;

my ($primersHashRef) = &readPrimers($primersFile);
my $primerLength;
{
	my @primers = keys %{$primersHashRef};
	$primerLength = length $primers[0];
}
my $seqWithoutPrimerSize = $minSeqSize - $primerLength;
$clusterThresh /= 100;
my $divergence           = 1 - $clusterThresh;
my $seedLength           = 33;
my $mismatches           = int( $seqWithoutPrimerSize * $divergence );

runClustering($Bin, $Config{bin_dir}, $clusterThresh);

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Subroutines -=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-

#-=-=-=-=-=-=--

#
# Step-by step clustering menagment.
#
sub runClustering
{
	my ($Bin, $bin_dir, $clusterThresh) = @_;

	# first clustering stage; identify identical sequences;
	my $trimCom = "$trimCluster $fastaFile $primersFile $clustersFile $seqsFile $minSeqSize $qualThresh";
	&executeCom($trimCom);

	### check if the output file is empty; abort if it is
	my $clusters_file_size = -s $clustersFile;
	my $seqs_file_size     = -s $seqsFile;
	unless ( -s $clustersFile && $seqsFile )
	{
		&abort_execution(
			"Failed to cluster. May be your primers do not match the dataset?");
		exit;
	}

	# run second level clustering, depending on the algorithm
	if ( $clusteringAlgorithm eq "u" )
	{
		runUclust($Bin, $bin_dir, $clusterThresh);
	}
	else
	{
		&runPyroclust($clusterThresh);
	}

	# classify clusters to the nearest neibour in the phylogenetic database
	my $phyloBlastFile = $repsFile . ".phyloblast";

#    my $phyloBlastDatabase = "/home/mepweb/referenceDb/pyrotag_bac_arc_euk_db.fa.select";
	my $phyloBlastDatabase = $Config{phyloBlastDB};

	my $phyloBlastCom =
	  "$bin_dir/blastn -word_size 90 -query $repsFile -num_alignments 1 -num_descriptions 1 -out $phyloBlastFile -db $phyloBlastDatabase -outfmt \"6 qseqid sacc pident length mismatch gapopen qstart qend sstart send evalue score\"";

# same result with legacy blast
#    my $phyloBlastCom = "blastall -p blastn -W 90 -i $repsFile -b 1 -m 8 -o $phyloBlastFile -d $phyloBlastDatabase ";
	&executeCom($phyloBlastCom);

# select sequences that did not have hits; fetch them and re-blast; add the results to the final thing
	my $seqsWithoutHitsFile = $phyloBlastFile . ".nohits";
	my $selectUnblastedSeqs =
	  "$Bin/selectUnblastedSeqs.pl  $phyloBlastFile $repsFile > $seqsWithoutHitsFile";
	&executeCom($selectUnblastedSeqs);

	my $reblastCom =
	  "$bin_dir/blastn -word_size 30 -query $seqsWithoutHitsFile -num_alignments 1 -num_descriptions 1 -db $phyloBlastDatabase -outfmt \"6 qseqid sacc pident length mismatch gapopen qstart qend sstart send evalue score\">>  $phyloBlastFile";

# same result with legacy blast
#    my $reblastCom = "/jgi/tools/bin/blastall -p blastn -W 30 -i $seqsWithoutHitsFile -b 1 -m 8 -d $phyloBlastDatabase >> $phyloBlastFile";
	&executeCom($reblastCom);

# blast output will contain only fragments of sequences - complement to the complete name
	my $giveNamesFile = $phyloBlastFile . ".names";
	my $giveNamesCom =
	  "$Bin/mapBlastOutputHist2database.pl $phyloBlastDatabase $phyloBlastFile > $giveNamesFile";
	&executeCom($giveNamesCom);

	my $potentialChimerasFile = $phyloBlastFile . ".chimeras";
	my $chimeraCheckCommand =
	  "$Bin/chimeraCheck.pl $phyloBlastFile $seqWithoutPrimerSize > $potentialChimerasFile";
	&executeCom($chimeraCheckCommand);

# make a table, in which columns are experiments separated by primers/barcodes
#                    and rows are OTUs, or clusters
#    my $produceTableCom = "$produceTableCommand $finalFasta $mclFile $clustersFile $primersFile $fastaFile $giveNamesFile > $tableFile";
	my $produceTableCom =
	  "$produceTableCommand $repsFile $superClustersFile $clustersFile $giveNamesFile $trimmedPrimerFastaMapFile $potentialChimerasFile $prefix > $tableFile";
	&executeCom($produceTableCom);
	my $clusters2reads = $prefix . ".clusters2reads.txt";

	my $parseTableCom =
	  "$Bin/parseTable.pl $tableFile 2 r $phyloGraphFile $phyloGraphFractionsFile";
	&executeCom($parseTableCom);

	# if email is present
	if ($email) { &compressAndSendEmail; }

	#    else{	&copyFilesAndCleanUp;}
}

#-=-=-= Other subroutines

sub executeCom
{
	my $com = shift @_;

	#    print STDERR $com, "\n";
	#    $com = "/usr/bin/time " . $com;
	print STDERR "Executing $com\n";
	my $startTime = time;
	open( COM, "$com |" ) || die "ERROR: can't execute $com\n";
	print STDERR <COM>;
	close COM;
	my $endTime   = time;
	my $time_diff = $endTime - $startTime;
	print STDERR "Done in $time_diff seconds\n";
}

sub runPyroclust
{
    my ($clusterThresh) = @_;
	# run align_pirotags script
	my $large_word_size;
	if   ( $minSeqSize < 300 ) { $large_word_size = 80; }     # FLX run
	else                       { $large_word_size = 150; }    # titanium rin
	my $alignedPyrotagsClusters = $seqsFile . ".pyroclusters";
	my $finalFasta              = $alignedPyrotagsClusters . ".seq";

	my $alignPyrotagsCom =
	  $pyroclustProg . " $seqsFile $large_word_size 6 $clusterThresh";
	&executeCom($alignPyrotagsCom);
	rename( $finalFasta,              $repsFile );
	rename( $alignedPyrotagsClusters, $superClustersFile );

}

sub runUclust
{
	my ($Bin, $bin_dir, $clusterThresh) = @_;
	my $uclust_results_file = $seqsFile . ".uclust";

	# run clustering
	my $band_width = int( 0.03 * $minSeqSize );
	my $uclustCom =
	  "$bin_dir/uclust --input $seqsFile --uc $uclust_results_file --id $clusterThresh --gapopen 1 --gapext 1 --mismatch -1 --maxrejects 15 --band $band_width --nucleo --norev";

	# convert to fasta
	&executeCom($uclustCom);

#	my $uclust2fasta = "uclust --uc2fasta $uclust_results_file --input $seqsFile --output results.fasta";
# make clusters 2 reads file
	my $reformat_uclust_com =
	  "$Bin/reformat_uclust.pl $uclust_results_file $seqsFile $repsFile $superClustersFile";
	&executeCom($reformat_uclust_com);
}

sub lucy_trim
{
	my ( $fastaFile, $qualFile, $bin_dir ) = @_;
	############
	#
	# Do lucy-trimming

	my $lucy_trim_com = "$Bin/lucyTrim.pl $fastaFile $qualFile 99.8 $Config{bin_dir}/lucy";
	&executeCom($lucy_trim_com);

	# from now on, the fasta file is the lucy-trimmed fasta
	# and we need to tell it to the other programs too!

	$fastaFile = $fastaFile . ".99.8.fasta";

	# check if the output file is empty; abort if it is
	my $outputFastaFileSize = -s $fastaFile;
	unless ( -s $fastaFile )
	{
		&abort_execution(
			"Failed to lucy-trim your data. Please verify your input data files"
		);
		exit;
	}
	return $fastaFile;
}

sub compressAndSendEmail
{

	# files to send to user:
	#    final table
	#    cluster representative sequences
	#    maping of final clusters to reads
	my $zipCom =
	  "gzip -9 $clusters2reads $tableFile $repsFile $fractions_file $datasets2reads $unifracInput $qcFastaFile $qcQualFile";

	my ($id) = $dir =~ /(\d+)\/?$/;
	unless ($id) { $id = ''; }

	&executeCom($zipCom);
	my $link_base = "$Config{cgi_url}/sendFile.pl?id=$id&fileName=";
	my $cluster_classification_link = $link_base . $tableFile . ".gz";
	my $cluster_classification_percent_link =
	  $link_base . $fractions_file . ".gz";
	my $phylum_classifications_link = $link_base . $phyloGraphFile;
	my $phylum_classification_percent_link =
	  $link_base . $phyloGraphFractionsFile;
	my $clusterRepresentatives_link = $link_base . $repsFile . ".gz";
	my $clusters2reads_link         = $link_base . $clusters2reads . ".gz";
	my $unifrac_link                = $link_base . $unifracInput . ".gz";
	my $datasets2reads_link         = $link_base . $datasets2reads . ".gz";
	my $error_console_link          = $link_base . "cluster.errors";
	my $qcFasta_link = $link_base . $qcFastaFile . ".gz";
	my $qcQual_link = $link_base . $qcQualFile . ".gz";

	#my $fractions_file = ".classification_fraction.xls"
	# mail the ziped file to the user if email is present
	my $msg = MIME::Lite->new(
		From    => 'eskirton@lbl.gov',
		To      => $email,
		Subject => 'Pyrotag processing results for run ' . $prefix,
		Type    => 'multipart/mixed', );
	$msg->attach(
		Type => 'TEXT/HTML',
		Data => "
Dear User,
<P>
Here are your pyrotag processing results from run $prefix. Please find links to several files. Some files are compressed using gzip program, you will need a file compression software to uncompress them.
</P><P>
<font color=red>Please download all the files now. Your files will be removed witin a few days to protect privacy of your data and to save disk space.</font>
</P><P>
The files <A HREF='$cluster_classification_link'>cluster_classification.xls</A> and <A HREF='$cluster_classification_percent_link'>cluster_classification_percent.xls</A> contain mapping of clusters (OTUs clustered at 97% identity by default) to datasets (per read or as dataset percent fractions respectively), as identified in your primer/barcode file. For each cluster the best hit in greengenes (for prokaryotes) and silva (for eukaryotes) databases is given, together with Blast alignment information in a concise (m8) format. Potential chimeras, if any, are specified in the bottom of these files (look for 'Potential chimeras').
</P><P>
The file <A HREF='$phylum_classifications_link'>phylum_classifications.xls</A> and <A HREF='$phylum_classification_percent_link'>phylum_classification_percent.xls</A> contain the distribution of read counts for each phylum found in each of your datasets (per read or as dataset percent fractions respectively).
</P><P>
The file <A HREF='$clusterRepresentatives_link'>clusterRepresentatives.fasta</A> contains representative sequences for each cluster.
</P><P>
The file <A HREF='$clusters2reads_link'>clusters2reads.txt</A> maps which reads were assigned to which cluster.
</P><P>
The file <A HREF='$unifrac_link'>unifrac.txt</A> can be used as an input file for weighted UniFrac analysis.
</P><P>
The file <A HREF='$datasets2reads_link'>datasets2reads.txt</A> contains mapping between datasets and reads.
</P><P>
The file <A HREF='$error_console_link'>cluster.errors</A> contains the execution consol of this run.
</P>
<HR>
<H3>Additional files:</H3>
<P>The file <A HREF='$qcFasta_link'>$prefix.fasta.gz</A> and <A HREF='$qcQual_link'>$prefix.fasta.qual.gz</A> are the QC'ed reads.</P>
<HR>
<P>
<B>Please site: <A HREF='http://www.theopenjournal.org/toj_articles/1'>Kunin V., Hugenholtz, P. PyroTagger: A fast, accurate pipeline for analysis of rRNA amplicon pyrosequence data. The Open Journal, Article 1, Feb 2010.</A></B>
</P><P>
Sincerely yours, 
</P><P>
      The PyroTagger team.
", );

	$msg->send;

	#    &copyFilesAndCleanUp;
}

sub abort_execution
{
	my ($error_message) = @_;
	if ($email)
	{
		my $sendmail = "/usr/sbin/sendmail ";
		my $subject  = "Subject: Pyrotags processing failed";

		#	my $file = "subscribers.txt";

		my $msg = MIME::Lite->new(
			From    => 'eskirton@lbl.gov',
			To      => $email,
			Subject => 'Pyrotag processing failed',
			Type    => 'multipart/mixed', );
		$msg->attach(
			Type => 'TEXT',
			Data =>
			  "Your submission to pyrotags pipeline failed.\nReason: $error_message",
		);

		$msg->send;
		exit;
	}
	else
	{

		#	&copyFilesAndCleanUp;
		die $error_message;
	}
}

sub copyFilesAndCleanUp
{
	if ( $dir && $original_dir )
	{
		my $errors_file = $cluster_dir . "cluster.errors";
		my $stdout_file = $cluster_dir . "cluster.stdout";
		unlink $errors_file;
		unlink $stdout_file;
		system "rsync -q -a $cluster_dir $original_dir";
		system "rm -rf $cluster_dir";
	}
}

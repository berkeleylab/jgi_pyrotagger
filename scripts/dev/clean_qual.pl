#!/usr/bin/env perl

use strict;
use warnings;

$|=1;

my ($inFile) = @ARGV;

unless ($inFile)
{
    die "
USAGE:
$0 inFile
";
}


open (IN, $inFile) || die "ERROR: can't open $inFile\n\a";
my @lines = <IN>;
my $all_lines = join("", @lines);
$all_lines .= "\n";
$all_lines =~ s/\n\n/\n/g;
print $all_lines;

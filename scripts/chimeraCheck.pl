#!/usr/bin/env perl

use warnings;
use strict;

$| = 1;

my ( $inFile, $seqLength ) = @ARGV;

unless ( $inFile && $seqLength )
{
	die "
USAGE:
$0 inFile seqLength
";
}

open( IN, $inFile ) || die "ERROR: can't open $inFile\n\a";

my $alignmentLengthThreshold = int( $seqLength * 0.9 );
print "Potential chimeras: ";
foreach my $line (<IN>)
{
	chomp $line;
	my ($query,      $hit,    $identity,   $alignmentLength,
		$mismatches, $gaps,   $queryStart, $queryEnd,
		$hitStart,   $hitEnd, $e_value,    $score
	) = split( /\t/, $line );

	if ( $identity > 90 && $alignmentLength < $alignmentLengthThreshold )
	{
		print "$query, ";
	}
}
close IN;
print "\n";

#!/usr/bin/env perl

use strict;
use warnings;

$| = 1;

my ( $inFile, $primersFile, $outputFile4clusters, $outputFile4seqs, $minSeqSize,
	$qualThresh )
  = @ARGV;

#-=-=-=-=-=-=-=-=-=-=-=- set constants -=-=-=--=-=-=-=-==-=-=-=-=-=

my $primer_length;

my $sequence_length;

#-=-=-=-=-=-=-=-=-=-=-=- set constants -=-=-=--=-=-=-=-==-=-=-=-=-=

unless ( $inFile
	&& $primersFile
	&& $outputFile4clusters
	&& $outputFile4seqs
	&& $minSeqSize
	&& $qualThresh )
{
	die "
USAGE:
$0 inFile primersFile outputFile4clusters outputFile4seqs minSeqSize badBasesFractionThreshold
";
}

#-=-=-=-=-=-=-=-=-=-=-=- read primers -=-=-=-=-=-=-=-=-=-=-=-=-=-

open( IN, $primersFile ) || die "ERROR: can't open $primersFile\n\a";
my %primersHash;
foreach my $line (<IN>)
{
	chomp $line;
	if ( $line =~ /^(\S+)\s+(\S+)/ )
	{
		my $primer             = uc $2;
		my $this_primer_length = length $2;
		unless ($primer_length)
		{
			$primer_length   = $this_primer_length;
			$sequence_length = $minSeqSize - $primer_length;
		}
		elsif ( $primer_length != $this_primer_length )
		{
			die
			  "Primers are of different length, aborting.\nPrimer: $line\nis $this_primer_length and should be $primer_length\n";
		}

		#	print $primer, "\t", $1, "\n";
		$primersHash{$primer} = $1;
	}
}

close IN;

#-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- if the qual file exists, read it -=-=-=-=-=-=-=-=-
my %badQualitySequences;
my $qualFile = $inFile . ".qual";
####### Quality filter is here:
&removeBadSeqs;

#&printTrimmedFata;
#exit;
#=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-- Read the fasta file -=-=-=-=-=-=-=-=-=-=-=-=-

open( IN, $inFile ) || die "ERROR: can't open $inFile\n\a";
my $seq_num      = 0;
my $good_seq_num = 0;

my $seq;
my $seqName = '';
my %seqHash;
while ( my $line = <IN> )
{
	chomp $line;
	if ( $line =~ /^>(\S+)/ )
	{
		$seq_num++;
		if ($seqName)
		{
			unless ( exists $badQualitySequences{$seqName} )
			{
				my $size = length $seq;
				if ( $size > $minSeqSize )
				{

					# trim primer and the length
					$seq = &trimPrimer($seq);
					if ($seq)
					{
						$seqHash{$seq}{$seqName} = 1;
						$good_seq_num++;
					}
				}
			}
		}
		$seqName = $1;
		$seq     = '';
	}
	else
	{
		$seq .= $line;
	}
}
close IN;
my $size = length $seq;

if ( $size > $minSeqSize )
{

	# trim primer and the length
	$seq = &trimPrimer($seq);
	if ($seq)
	{
		$seqHash{$seq}{$seqName} = 1;
		$good_seq_num++;
	}
}
my @seqs = keys %seqHash;
@seqs = sort {
	( scalar keys %{ $seqHash{$b} } ) <=> ( scalar keys %{ $seqHash{$a} } )
} @seqs;
my $counter = 0;

print STDERR "Initial number of sequences: ",   $seq_num,      "\n";
print STDERR "Number of sequences clustered: ", $good_seq_num, "\n";
print STDERR "Number of 100% identity clusters: ", scalar @seqs, "\n";

#<STDIN>;

my $seqs_file = $outputFile4seqs;

open( CLUSTERS, ">$outputFile4clusters" )
  || die "ERROR: can't open $outputFile4clusters for writing\n";

open( SEQS, ">$seqs_file" ) || die "ERROR: can't open $seqs_file for writing\n";

foreach my $seq (@seqs)
{
	$counter++;
	&print2File( $seq, ">Cluster" . $counter );
	print CLUSTERS "Cluster" . $counter;
	my @seqNames = keys %{ $seqHash{$seq} };
	&print2File( $seq, " ", scalar @seqNames );
	print CLUSTERS " ", scalar @seqNames, "\t";

	foreach my $seqName (@seqNames)
	{
		print CLUSTERS $seqName, "\t";

	}
	&print2File( $seq, "\n" . $seq . "\n" );
	print CLUSTERS "\n";
}
close CLUSTERS;
close SEQS;

sub print2File
{
	my ( $seq, $string ) = @_;
	my $seqLength = length $seq;
	if ( $seqLength == $sequence_length )
	{
		print SEQS $string;
	}
	else
	{
		die "ERROR: sequence of wrong length $seqLength!\n";
	}
}

sub trimPrimer
{
	my ($seq) = @_;
	my $primer = substr( $seq, 0, $primer_length );

	my $trimmedSeq;
	if ( exists $primersHash{$primer} )
	{
		$trimmedSeq = substr( $seq, $primer_length, $sequence_length );
	}

	return $trimmedSeq;
}

sub removeBadSeqs
{
	if ( -s $qualFile )
	{
		my $startTime = time;
		open( IN, $qualFile ) || die "ERROR: can't open $qualFile\n\a";
		print STDERR "Reading Qual file\n";
		my $shortSequences      = 0;
		my $lowQualitySequences = 0;

		my $seqName;

		#	my $badBases=0;
		my @qualScores;
		my $qualityThreshold  = 27;
		my $badBasesThreshold = int( $minSeqSize * $qualThresh / 100 );
		while ( my $line = <IN> )
		{
			chomp $line;
			if ( $line =~ /^>(\S+)/ )
			{
				if ($seqName)
				{
					chomp $line;
					my $length   = scalar @qualScores;
					my $badBases = 0;
					unless ( $length < $minSeqSize )
					{
						for ( my $i = 0; $i < $minSeqSize; $i++ )
						{
							my $qualScore = $qualScores[$i];
							unless ( defined $qualScore ) { last; }
							elsif ( $qualScore < $qualityThreshold )
							{
								$badBases++;
							}
						}
					}
					else
					{
						$shortSequences++;
						$badQualitySequences{$seqName} = 1;
					}
					if ( $badBases > $badBasesThreshold )
					{
						$lowQualitySequences++;
						$badQualitySequences{$seqName} = 1;
					}

				}
				$seqName    = $1;
				@qualScores = ();

				#		$badBases=0;
			}
			else
			{
				my @lineArr = split /\s+/, $line;
				push( @qualScores, @lineArr );
			}
		}

		if ($seqName)
		{
			my $badBases = 0;
			my $length   = scalar @qualScores;
			if ( $length >= $minSeqSize )
			{
				for ( my $i = 0; $i < $minSeqSize; $i++ )
				{
					my $qualScore = $qualScores[$i];
					unless ( defined $qualScore )             { last; }
					elsif  ( $qualScore < $qualityThreshold ) { $badBases++; }
				}
			}
			else
			{
				$shortSequences++;
				$badQualitySequences{$seqName} = 1;
			}
			if ( $badBases > $badBasesThreshold )
			{
				$lowQualitySequences++;
				$badQualitySequences{$seqName} = 1;
			}

		}
		close IN;
		my $endTime     = time;
		my $runningTime = $endTime - $startTime;
		print STDERR
		  "Sequences discarded: $lowQualitySequences were low quality and $shortSequences were too short. This filtering took $runningTime seconds.\n";
	}
}

sub printTrimmedFata
{
	open( IN, $inFile ) || die "ERROR: can't open $inFile\n\a";
	my $seq_num      = 0;
	my $good_seq_num = 0;
	my $printIt      = 0;
	my $outFile      = $inFile . ".trimmed";
	open( OUT, ">$outFile" ) || die "ERROR: can't write to $outFile\n";

	while ( my $line = <IN> )
	{
		if ( $line =~ /^>(\S+)/ )
		{
			my $seqName = $1;
			$seq_num++;
			unless ( exists $badQualitySequences{$seqName} )
			{
				print OUT $line;
				$printIt = 1;
				$good_seq_num++;
			}
			else
			{
				$printIt = 0;
			}
		}
		elsif ( $printIt == 1 )
		{
			print OUT $line;
		}
	}
	close IN;
	close OUT;
	print STDERR "$seq_num sequences read, $good_seq_num saved\n";
}

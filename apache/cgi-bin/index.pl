#!/usr/bin/env perl

##############################################################
#
# Pyrotag processing pipeline, web version
#
# By Victor Kunin, Feb 2009. email: vkunin@lbl.gov
#
#
###############################################################

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Modueles -=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-

use strict;
use warnings;
use Email::Valid;
use MIME::Lite;
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use FindBin qw($Bin);
use lib "$Bin/../../perl/share/perl/5.10.1";
use Config::Simple;
use File::Copy;

$CGITempFile::TMPDIRECTORY = '/scratch/www/pyrotagger/output';

# CONFIG
my %Config;
Config::Simple->import_from( "$Bin/../../pyrotagger.ini", \%Config );
die("Outputs dir not defined\n") unless exists( $Config{output_dir} );
die("Outputs dir does not exist\n") unless -d $Config{output_dir};

# CGI PARAMETERS
my $state               = param('state');
my $fasta1              = param('fastafile1');
my $qual1               = param('qualfile1');
my $primers1            = param('primersfile1');
my $table_rows          = param("table_rows");
my $minSeqSize          = param('MinSeqSize');
my $email               = param('email');
my $id                  = param('id');
my $prefix              = param('prefix');
my $qualThresh          = param('qualThresh');
my $clusterThresh       = param('clusterThresh');
my $clusteringAlgorithm = param('clusteringAlgorithm');
$prefix =~ s/\s+/_/g if $prefix;

# MAIN
if ( $state == 4 ) # $minSeqSize and $email and $id )
{

	# If we have every data we need and can start the main pipeline
	state4( $Config{scripts_dir}, "$Config{output_dir}/$id", $prefix, $id, $clusteringAlgorithm, $qualThresh, $clusterThresh, $email, $minSeqSize );
}
elsif ( $state == 3 )
{
    state3($id, $prefix, $qualThresh, $clusterThresh, $email, $Config{cgi_url});
}
elsif ( $state == 2 ) # defined($id) and $id )
{

	# We do not have all the data
	# render Checks Waiting Page
    state2("$Config{output_dir}/$id", $id, $prefix, $qualThresh, $clusterThresh, $email, $Config{cgi_url});
}
elsif ( $state == 1 ) #  $fasta1 and $qual1 and $primers1 )
{

	# we have the files, do all the intial checks
	state1( $Config{scripts_dir}, $Config{output_dir}, $prefix, $qualThresh, $clusterThresh, $email, $table_rows, $Config{cgi_url} );
}
else
{

	# we have no data at all, print the form
	state0($table_rows);
}
exit;

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Subroutines  -=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-

sub state4
{
	my ( $Bin, $an_output_dir, $prefix, $id, $clusteringAlgorithm, $qualThresh,
        $clusterThresh, $email, $minSeqSize ) = @_;

	die("Output dir not defined\n") unless $an_output_dir;
	die("Output dir does not exist\n") unless -d $an_output_dir;
    die("Id not defined\n") unless $id;
    die("Invalid clustering algorithm: $clusteringAlgorithm\n") unless $clusteringAlgorithm =~ /^[pu]$/;
    die("Quality threshold not defined\n") unless $qualThresh;
    die("Invalid quality threshold: $qualThresh\n") unless $qualThresh =~ /^\d{1,2}$/;
    die("Cluster threshold not defined\n") unless $clusterThresh;
    die("Invalid cluster threshold: $clusterThresh\n") unless $clusterThresh =~ /^9\d$/;
    die("Invalid email: $email\n") unless $email and $email =~ /^\S+@\S+\.\w+$/;

	if ($prefix)
	{
		if   ( $prefix =~ /^([\w_\.]+)/ ) { $prefix = $1; }
		else                              { $prefix = $id; }
	} else
    {
        $prefix = $id;
    }

	# block user from pressing a 'refresh' button and re-run the submission.
	my $resubmissionBlock = "$an_output_dir/resubmissionBlock";
	if ( -e $resubmissionBlock )
	{
		printSubmissionNotice();
		return;
	}
	system("touch $resubmissionBlock");

	my $command =
	  "$Bin/runPipeline.pl -s $minSeqSize -d $an_output_dir -e $email -f $an_output_dir/primers2fasta.txt -p $prefix -q $qualThresh -i $clusterThresh -a $clusteringAlgorithm";
	system("echo $command >> $an_output_dir/pipelineCommands.sh");
	system("nohup $command >& $an_output_dir/cluster.errors &");
	printSubmissionNotice($command);
}

sub state1
{
	my ( $Bin, $an_output_dir, $prefix, $qualThresh, $clusterThresh, $email, $table_rows, $url ) = @_;

    # VALIDATE INPUT
	die("Output dir not defined") unless $an_output_dir;
	die("Output dir does not exist") unless -d $an_output_dir;
    die("Qual threshold not defined") unless $qualThresh;
    die("Cluster threshold not defined") unless $clusterThresh;
    die("A valid email address is required\n") unless $email;
    die("The email address you provided is not valid: $email\n") unless Email::Valid->address($email);
	$table_rows = 3 unless $table_rows and $table_rows =~ /^\d+$/;

    # INIT VARS
	my $id = $$;    # use pid as unique id
	my $dir = "$an_output_dir/$id/";
	mkdir $dir or die("ERROR: can't create directory $dir: $!");
	chmod 0777, $dir;
	chdir $dir;

    # SAVE FILES AND CREATE INDEX FILE
	my $files_index_file = $dir . "files_index";
	open( FILES_INDEX, ">$files_index_file" ) or die("Can't open $files_index_file for writing\n");
	for ( my $i = 1; $i <= $table_rows; $i++ )
	{
		# save files
		my $primers = param( 'primersfile' . $i );
		my $fasta   = param( 'fastafile' . $i );
		my $qual    = param( 'qualfile' . $i );
        last unless $fasta or $qual or $primers;
		die("Please provide primers, fasta and qual files\n") unless $fasta and $qual and $primers;

		my $fastaFile   = save_file( $fasta,   $i . ".fasta",          $dir );
		my $qualFile    = save_file( $qual,    $i . ".fasta.qual",     $dir );
		my $primersFile = save_file( $primers, $i . ".primers_wobble", $dir );

		my $primersFileSize = -s $primersFile;
		if ( $primersFileSize > 4096 )
		{
            die("ERROR: your primers file $primers is too big ($primersFileSize bytes). Maximum size of a primers file is 4Kb");
		}
		print FILES_INDEX $primersFile, "\t", $fastaFile, "\t", $qualFile, "\n";
	}
	close FILES_INDEX;

    # RUN INITIAL CHECKS SCRIPT
	system("nohup $Bin/initial_checks.pl $qualThresh $files_index_file $dir &> checks_errors &");

    # REDIRECT
    my $newLocation = "$url/index.pl?id=$id&email=$email&prefix=$prefix&qualThresh=$qualThresh&clusterThresh=$clusterThresh&state=2";
    print redirect($newLocation);

    # Saves user-submitted file
    sub save_file
    {
        my ( $file, $type, $dir ) = @_;
        die unless $file and $type and $dir;

        my $final_filename = $dir . $type;
        my $filename       = $final_filename;

        if    ( $file =~ /\.gz$/ )  { $filename .= ".gz" }
        elsif ( $file =~ /\.zip$/ ) { $filename .= ".zip" }
        elsif ( $file =~ /\.bz2$/ ) { $filename .= ".bz2" }

        move( tmpFileName($file), $filename );
        die("ERROR saving $file; received 0 bytes.\n") unless -s $filename;
        return $filename;
    }
}

# render checks waiting page
sub state2
{
    my ($dir, $id, $prefix, $qualThresh, $clusterThresh, $email, $url) = @_;
    die("Dir not defined") unless $dir;
    die("ID not defined") unless $id;
    die("Qual threshold not defined") unless $qualThresh;
    die("Cluster threshold not defined") unless $clusterThresh;
    die("Email not defined") unless $email;
    die("URL not defined") unless $url;

	my $checks_success_file = "$dir/checks_successful";
	my $checks_failed_file  = "$dir/checks_failed";
	my $checks_errors_file  = "$dir/checks_errors";
	if ( -e $checks_success_file )
	{
        print redirect("$url/index.pl?id=$id&email=$email&prefix=$prefix&qualThresh=$qualThresh&clusterThresh=$clusterThresh&state=3");
	}
	elsif ( -e $checks_failed_file )
	{
		print header, start_html(-title=>'PyroTagger'),
        h1("The initial checks failed."), p("Here is the output of the checks progress console"), "<pre>\n";
		open( ERR, '<', $checks_errors_file );
		print <ERR>;
		close ERR;
		print "</pre>\n", end_html;
	}
	else
	{
        print header(-Refresh => "15;url=$url/index.pl?id=$id&email=$email&prefix=$prefix&qualThresh=$qualThresh&clusterThresh=$clusterThresh&state=2"),
        start_html(-title=>'PyroTagger'), h2("We are running initial checks on your data (run id = $id)"), 
        p("This page will refresh every 15 seconds"), end_html;
	}
}

# print read size distribution page
sub state3
{
    my ($id, $prefix, $qualThresh, $clusterThresh, $email, $url) = @_;

    # VALIDATE INPUT
    die("ID not defined") unless $id;
    die("Qual threshold not defined") unless $qualThresh;
    die("Cluster threshold not defined") unless $clusterThresh;
    die("Email not defined") unless $email;

    # PRINT FORM
	print header, start_html(-title=>'PyroTagger'), <<EOF;
<BR>
<P>
<FORM>
Choose a clustering algorithm
<select name="clusteringAlgorithm">
  <option value='u'>uclust</option>
  <option value='p'>pyroclust</option>
</select><BR>
Based on the chart below choose read size threshold
<INPUT type=TEXT size=4 name='MinSeqSize'>
<BR>
<INPUT TYPE=HIDDEN name='state' value=4>
<INPUT TYPE=HIDDEN name='id' value=$id>
<INPUT TYPE=HIDDEN name='email' value=\'$email\'>
<INPUT TYPE=HIDDEN name='qualThresh' value=\'$qualThresh\'>
<INPUT TYPE=HIDDEN name='clusterThresh' value=\'$clusterThresh\'>
<INPUT TYPE=HIDDEN name='prefix' value=\'$prefix\'>
<BR>
<INPUT TYPE=SUBMIT>
</FORM>
<BR>
<IMG src=\"$url/readSizeDistribution.pl?id=$id&readHash=true\">
</BODY>
</HTML>
EOF
}

# Prints the web page with the form.
sub state0
{
	my ($table_rows, $error_string) = @_;
	$table_rows = 3 unless $table_rows and $table_rows =~ /^\d+$/;
    print header, start_html(-title=>'PyroTagger');
	if ($error_string)
	{
		print "<h3>\n<font color=red>$error_string</font>\n</h3>\n";
	}

	print <<EOF;
<P>
Upload mapping file (see below for format description), fasta-formatted sequence file and qual file.
<BR>
Files can be compressed using gzip, bzip2, or zip (must have .gz|bz2|zip extension).
</P><P>
<A HREF="/help.html">Help!</A>
</P>
<P>
<FORM enctype="multipart/form-data" method=POST>
<TABLE>
<TR><TD><B>Mapping file</B></TD><TD><B>Fasta file</B></TD><TD><B>Qual file</B></TD></TR>
EOF
	for ( my $i = 1; $i <= $table_rows; $i++ )
	{
		print "
<TR>
   <TD><INPUT SIZE=20 NAME=\"primersfile" . $i . "\" TYPE=\"file\"></TD>
   <TD><INPUT SIZE=20 NAME=\"fastafile" . $i . "\" TYPE=\"file\"></TD>
   <TD><INPUT SIZE=20 NAME=\"qualfile" . $i . "\" TYPE=\"file\"></TD>
</TR>
";
	}
	print <<EOF;
</TABLE>
<P>
<TABLE>
<TR><TD>Email address to send the results</TD><TD><INPUT type=TEXT size=20 name=email> <font size=-2 color=red><I>required</I></font></TD></TR>
<TR><TD>Give a one-word name for this run<BR> (underscores and dots are allowed)</TD><TD><INPUT type=TEXT size=20 name=prefix> </TD></TR>
<TR><TD>Threshold for % of low quality bases</TD><TD>
<select name="qualThresh">
  <option>3</option>
  <option>5</option>
  <option selected>10</option>
  <option>15</option>
  <option>99</option>
</select>
<font size=-2>Use 3 for FLX, 10 for Titanim</font>
<TR><TD>Sequence similarity % for clustering</TD><TD>
<select name="clusterThresh">
  <option>95</option>
  <option>96</option>
  <option selected>97</option>
  <option>98</option>
  <option>99</option>
</select>
</TD></TR>

</TABLE>
<INPUT type=HIDDEN name=table_rows value=$table_rows>
<INPUT type=HIDDEN name=state value=1>
</P>
<P>
<INPUT TYPE=SUBMIT>
<A HREF="/help.html">Help!</A>
</FORM>
<FORM method=POST>
I need the data upload table to have <INPUT TYPE=TEXT SIZE=1 NAME=table_rows> rows.<INPUT TYPE=SUBMIT VALUE="GO">
</FORM>
</P>
<HR>
<P>
Example of <b>mapping file</b> content: <br />
__________ <br />
<pre>
Sample1    CTACTacgggcggtgtgtRc
Sample2    CTCGCacgggcggtgtgtYc
</pre>
__________ <br />
</P><P>
Rules: <br />
<ul>
<li>Each line should contain sample name, followed by a tab, followed by barcode-primer sequence. 
<li>No other lines/info should be included in the file.
<li>All barcode-primer sequences should be the same length.
<li>No spaces are allowed in the primer name.
<li>Barcode-primer sequence should not contain 454 adaptor, and should start from the barcode.
<li>Allowed wobbles are B,D,H,K,M,N,R,S,V,W,X,Y.
<li>Sequence can be lowercase, uppercase or any mixture of the two.
</ul>
</P>
<HR>
<font size=-1><I>By Victor Kunin</font></I>
</BODY>
</HTML>
EOF
}

sub printSubmissionNotice
{
	my ($command) = @_;
	print header, start_html(-title=>'PyroTagger'), <<EOF;
<H2>We received your request</H2>
<BR>
<HR>
<BR>
Your request has been submitted. You should receive an email with the results. Depending on the size and complexity of the dataset, the email should arrive within a few minutes to hours.
<P>
For your reference, the id of your request is <font color=green>$id</font>.
</P><P>
You can monitor the run by checking the 
<A HREF="sendFile.pl?id=$id&fileName=cluster.errors">progress console</A><BR>
<HR>

<font size=-1><I>By Victor Kunin</font></I>
</BODY>
</HTML>
EOF
}

